---
layout: ontology_detail
id: perosand
title: Perovskite based solar cell manufacturing ontology
jobs:
  - id: https://travis-ci.org/mohamed.koubaa/peosand
    type: travis-ci
build:
  checkout: git clone https://github.com/mohamed.koubaa/peosand.git
  system: git
  path: "."
contact:
  email: 
  label: 
  github: 
description: Perovskite based solar cell manufacturing ontology is an ontology...
domain: stuff
homepage: https://github.com/mohamed.koubaa/peosand
products:
  - id: perosand.owl
    name: "Perovskite based solar cell manufacturing ontology main release in OWL format"
  - id: perosand.obo
    name: "Perovskite based solar cell manufacturing ontology additional release in OBO format"
  - id: perosand.json
    name: "Perovskite based solar cell manufacturing ontology additional release in OBOJSon format"
  - id: perosand/perosand-base.owl
    name: "Perovskite based solar cell manufacturing ontology main release in OWL format"
  - id: perosand/perosand-base.obo
    name: "Perovskite based solar cell manufacturing ontology additional release in OBO format"
  - id: perosand/perosand-base.json
    name: "Perovskite based solar cell manufacturing ontology additional release in OBOJSon format"
dependencies:
- id: ro
- id: bfo

tracker: https://github.com/mohamed.koubaa/peosand/issues
license:
  url: http://creativecommons.org/licenses/by/3.0/
  label: CC-BY
activity_status: active
---

Enter a detailed description of your ontology here. You can use arbitrary markdown and HTML.
You can also embed images too.

